using System.Data;
using HotelBooking.Domain;
using HotelBooking.Ports;
using Microsoft.Data.Sqlite;

namespace HotelBooking.Infrastructure;

public class SQLiteHotelRepository : HotelRepository
{
    private readonly string _connectionString;

    public SQLiteHotelRepository(string connectionString)
    {
        _connectionString = connectionString;
    }
    
    public Hotel? Get(Guid id)
    {
        const string sql = "SELECT Id, Name FROM Hotel WHERE Id = @id";
        
        using var connection = new SqliteConnection(_connectionString);
        using var command = new SqliteCommand(sql, connection);
        
        command.Parameters.AddWithValue("@id", id);
        
        connection.Open();
        var dataReader = command.ExecuteReader();
        
        return MapHotelFrom(dataReader);
    }

    public void Save(Hotel hotel)
    {
        using var connection = new SqliteConnection(_connectionString);
        connection.Open();
        
        if(Exists(hotel, connection)) 
            Update(hotel, connection);
        else 
            Insert(hotel, connection);
    }
    
    private static Hotel? MapHotelFrom(IDataReader dataReader)
    {
        if(! dataReader.Read())
            return null;
        
        return new Hotel(
            dataReader.GetGuid(dataReader.GetOrdinal("Id")),
            dataReader.GetString(dataReader.GetOrdinal("Name"))
        );
    }
    
    private static bool Exists(Hotel hotel, SqliteConnection connection)
    {
        const string sql = "SELECT 1 FROM Hotel WHERE Id = @id";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@id", hotel.Id);
        
        var result = command.ExecuteScalar();
        return result != null && (long)result == 1;
    }
    
    private static void Insert(Hotel hotel, SqliteConnection connection)
    {
        const string sql = "INSERT INTO Hotel (Id, Name) VALUES (@id, @name)";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@id", hotel.Id);
        command.Parameters.AddWithValue("@name", hotel.Name);

        command.ExecuteNonQuery();
    }
    
    private static void Update(Hotel hotel, SqliteConnection connection)
    {
        const string sql = "UPDATE Hotel SET Name = @name WHERE Id = @id";
        
        using var command = new SqliteCommand(sql, connection);
        command.Parameters.AddWithValue("@id", hotel.Id);
        command.Parameters.AddWithValue("@name", hotel.Name);

        command.ExecuteNonQuery();
    }
}