using HotelBooking.Ports;

namespace HotelBooking.Domain;

public class ChangeHotelNameHandler
{
    private readonly HotelRepository _hotelRepository;

    public ChangeHotelNameHandler(HotelRepository hotelRepository)
    {
        _hotelRepository = hotelRepository;
    }

    public void ChangeHotelName(Guid hotelId, string newHotelName)
    {
        var hotel = _hotelRepository.Get(hotelId);
        if (hotel == null) {
            throw new UnknownHotelException("Unknown hotel.");
        }

        hotel.ChangeName(newHotelName);
        _hotelRepository.Save(hotel);
    }
}

public class UnknownHotelException : Exception
{
    public UnknownHotelException(string message) 
        : base(message)
    {}
}