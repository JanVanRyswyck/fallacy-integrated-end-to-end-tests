namespace HotelBooking.Domain;

public class Hotel
{
    public Guid Id { get; }
    public string Name { get; private set; }

    public Hotel(Guid id, string name)
    {
        Id = id;
        Name = name;
    }

    public void ChangeName(String name)
    {
        Name = name;
    }
}