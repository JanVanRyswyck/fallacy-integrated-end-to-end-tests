using HotelBooking.Domain;

namespace HotelBooking.Ports;

public interface HotelRepository
{
    // Retrieves a hotel for a specified identifier.
    // When found, returns a `Hotel` instance; otherwise a null reference is returned.
    Hotel? Get(Guid id);

    // Saves the data of the specified `Hotel` instance.
    void Save(Hotel hotel);
}