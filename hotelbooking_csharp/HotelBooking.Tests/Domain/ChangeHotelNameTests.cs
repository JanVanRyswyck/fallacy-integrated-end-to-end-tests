using HotelBooking.Domain;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace HotelBooking.Tests.Domain;

public class ChangeHotelNameTests
{
    [Test]
    public void Should_change_the_hotel_name() 
    {
        var hotelId = Guid.Parse("33701626-ad1c-4a30-9fd0-4cb891ac442c");
        var hotel = new Hotel(hotelId, "The Sphere");

        var hotelRepository = new FakeHotelRepository();
        hotelRepository.Save(hotel);

        var handler = new ChangeHotelNameHandler(hotelRepository);
        handler.ChangeHotelName(hotelId, "The New Sphere");

        var storedHotel = hotelRepository.Get(hotelId);
        Assert.That(storedHotel!.Name, Is.EqualTo("The New Sphere"));
    }

    [Test]
    public void Should_throw_exception_for_an_unknown_hotel() 
    {
        var hotelId = Guid.Parse("f688a902-c2f7-48c3-9cf2-29b4b190e21f");
        var handler = new ChangeHotelNameHandler(new FakeHotelRepository());

        var changeHotelName = () => handler.ChangeHotelName(hotelId, "The New Sphere");
        Assert.That(changeHotelName, Throws.InstanceOf<UnknownHotelException>());
    }
}