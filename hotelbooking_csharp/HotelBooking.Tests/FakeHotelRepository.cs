using HotelBooking.Domain;
using HotelBooking.Ports;

namespace HotelBooking.Tests;

public class FakeHotelRepository : HotelRepository
{
    private readonly Dictionary<Guid, Hotel> _hotels;

    public FakeHotelRepository()
    {
        _hotels = new Dictionary<Guid, Hotel>();
    }
    
    public Hotel? Get(Guid id)
    {
        return _hotels.GetValueOrDefault(id);
    }

    public void Save(Hotel hotel)
    {
        _hotels[hotel.Id] = hotel;
    }
}