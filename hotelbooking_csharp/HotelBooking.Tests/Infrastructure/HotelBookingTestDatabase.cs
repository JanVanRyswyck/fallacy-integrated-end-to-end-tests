using System.Reflection;

namespace HotelBooking.Tests.Infrastructure;

public class HotelBookingTestDatabase
{
    private readonly SQLiteDatabasePopulator _databasePopulator;

    public HotelBookingTestDatabase(string connectionString)
    {
        _databasePopulator = new SQLiteDatabasePopulator(connectionString);
    }
    
    public void Initialise()
    {
        var soccerClubSchema = ReadSqlResource("HotelBooking.Database.InitialiseSchema.sql");
        _databasePopulator.Execute(soccerClubSchema);
    }

    public void Cleanup()
    {
        var cleanupSchema = ReadSqlResource("HotelBooking.Database.CleanupSchema.sql");
        _databasePopulator.Execute(cleanupSchema);
    }
    
    private static string ReadSqlResource(string sqlFileName)
    {
        var mainAssembly = Assembly.GetAssembly(typeof(Program));
        if(mainAssembly == null)
            throw new InvalidOperationException("Unable to find the HotelBooking assembly.");
        
        using var resourceStream = mainAssembly.GetManifestResourceStream(sqlFileName);
        if(resourceStream == null)
            throw new FileNotFoundException($"Unable to read embedded SQL file with name '{sqlFileName}'.");
        
        using var streamReader = new StreamReader(resourceStream);
        return streamReader.ReadToEnd();
    }
}