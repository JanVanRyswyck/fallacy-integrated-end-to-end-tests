namespace HotelBooking.Tests.Infrastructure;

public class SQLiteTestConnectionUrl
{
    private SQLiteTestConnectionUrl()
    {}
    
    public static string Build()
    {
        var sqliteDatabaseFile = Path.Combine(Path.GetTempPath(), "HotelBookingTest.db");
        return $"Data Source={sqliteDatabaseFile};Foreign Keys=True";
    }
}