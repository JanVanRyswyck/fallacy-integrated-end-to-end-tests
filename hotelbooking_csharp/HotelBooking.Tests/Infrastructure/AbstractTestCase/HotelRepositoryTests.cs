using DeepEqual.Syntax;
using HotelBooking.Domain;
using HotelBooking.Ports;
using NUnit.Framework;

namespace HotelBooking.Tests.Infrastructure.AbstractTestCase;

public abstract class HotelRepositoryTests
{
    private HotelRepository _sut;

    [SetUp]
    public void SetUp()
    {
        Initialise();
        _sut = GetSubjectUnderTest();
    }

    [TearDown]
    public void TearDown()
    {
        Cleanup();
    }
    
    [Test]
    public void Should_return_hotel_data_for_identifier()
    {
        var hotelId = Guid.Parse("f24d22fc-25b6-47d6-ad52-07f624b462eb");
        var expectedHotel = new Hotel(hotelId, "Hotel Europe");
        _sut.Save(expectedHotel);

        var retrievedHotel = _sut.Get(hotelId);
        retrievedHotel.ShouldDeepEqual(expectedHotel);
    }
    
    [Test]
    public void Should_return_nothing_for_a_non_existing_hotel()
    {
        var unknownHotelId = Guid.Parse("9529f607-b1d2-4597-9b7e-26e4ba29d196");
        var retrievedHotel = _sut.Get(unknownHotelId);
        Assert.That(retrievedHotel, Is.Null);
    }
    
    [Test]
    public void Should_save_new_hotel_data()
    {
        var hotelId = Guid.Parse("f24d22fc-25b6-47d6-ad52-07f624b462eb");
        var newHotel = new Hotel(hotelId, "Hotel Europe");
        _sut.Save(newHotel);

        var retrievedHotel = _sut.Get(hotelId);
        retrievedHotel.ShouldDeepEqual(newHotel);
    }
    
    [Test]
    public void Should_save_existing_hotel_data()
    {
        var hotelId = Guid.Parse("4204ace3-8497-4c4b-8a23-91d874781b36");
        var newHotel = new Hotel(hotelId, "Hotel Europe");
        _sut.Save(newHotel);

        var changedHotel = new Hotel(hotelId, "Hilton");
        _sut.Save(changedHotel);

        var retrievedHotel = _sut.Get(hotelId);
        retrievedHotel.ShouldDeepEqual(changedHotel);
    }

    protected abstract void Initialise();
    protected abstract HotelRepository GetSubjectUnderTest();
    protected abstract void Cleanup();
}