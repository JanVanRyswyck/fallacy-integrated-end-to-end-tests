using HotelBooking.Ports;

namespace HotelBooking.Tests.Infrastructure.AbstractTestCase;

public class FakeHotelRepositoryTests : HotelRepositoryTests
{
    private readonly FakeHotelRepository _fakeHotelRepository;

    public FakeHotelRepositoryTests()
    {
        _fakeHotelRepository = new FakeHotelRepository();
    }

    protected override void Initialise()
    {}

    protected override HotelRepository GetSubjectUnderTest()
    {
        return _fakeHotelRepository;
    }

    protected override void Cleanup()
    {}
}