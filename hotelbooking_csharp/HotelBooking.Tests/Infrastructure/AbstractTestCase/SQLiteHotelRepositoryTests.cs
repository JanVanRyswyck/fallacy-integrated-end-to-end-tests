using HotelBooking.Infrastructure;
using HotelBooking.Ports;

namespace HotelBooking.Tests.Infrastructure.AbstractTestCase;

public class SQLiteHotelRepositoryTests : HotelRepositoryTests
{
    private readonly HotelBookingTestDatabase _hotelBookingDatabase;
    private readonly SQLiteHotelRepository _sqliteHotelRepository;

    public SQLiteHotelRepositoryTests()
    {
        var sqliteConnectionString = SQLiteTestConnectionUrl.Build();
        
        _hotelBookingDatabase = new HotelBookingTestDatabase(sqliteConnectionString);
        _sqliteHotelRepository = new SQLiteHotelRepository(sqliteConnectionString);
    }

    protected override void Initialise()
    {
        _hotelBookingDatabase.Initialise();
    }

    protected override HotelRepository GetSubjectUnderTest()
    {
        return _sqliteHotelRepository;
    }

    protected override void Cleanup()
    {
        _hotelBookingDatabase.Cleanup();
    }
}