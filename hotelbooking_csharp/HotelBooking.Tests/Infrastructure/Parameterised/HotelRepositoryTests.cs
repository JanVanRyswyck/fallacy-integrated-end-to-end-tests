using System.Collections;
using DeepEqual.Syntax;
using HotelBooking.Domain;
using HotelBooking.Infrastructure;
using HotelBooking.Ports;
using NUnit.Framework;

namespace HotelBooking.Tests.Infrastructure.Parameterised;

public class HotelRepositoryTests
{
    [TestCaseSource(nameof(HotelRepositoryStrategies))]
    public void Should_return_hotel_data_for_identifier(HotelRepositoryStrategy strategy)
    {
        using var _ = strategy.Initialise();
        var sut = strategy.GetSubjectUnderTest();
        
        var hotelId = Guid.Parse("f24d22fc-25b6-47d6-ad52-07f624b462eb");
        var expectedHotel = new Hotel(hotelId, "Hotel Europe");
        sut.Save(expectedHotel);

        var retrievedHotel = sut.Get(hotelId);
        retrievedHotel.ShouldDeepEqual(expectedHotel);
    }

    [TestCaseSource(nameof(HotelRepositoryStrategies))]
    public void Should_return_nothing_for_a_non_existing_hotel(HotelRepositoryStrategy strategy)
    {
        using var _ = strategy.Initialise();
        var sut = strategy.GetSubjectUnderTest();
        
        var unknownHotelId = Guid.Parse("9529f607-b1d2-4597-9b7e-26e4ba29d196");
        var retrievedHotel = sut.Get(unknownHotelId);
        Assert.That(retrievedHotel, Is.Null);
    }
    
    [TestCaseSource(nameof(HotelRepositoryStrategies))]
    public void Should_save_new_hotel_data(HotelRepositoryStrategy strategy)
    {
        using var _ = strategy.Initialise();
        var sut = strategy.GetSubjectUnderTest();
        
        var hotelId = Guid.Parse("f24d22fc-25b6-47d6-ad52-07f624b462eb");
        var newHotel = new Hotel(hotelId, "Hotel Europe");
        sut.Save(newHotel);
    
        var retrievedHotel = sut.Get(hotelId);
        retrievedHotel.ShouldDeepEqual(newHotel);
    }
    
    [TestCaseSource(nameof(HotelRepositoryStrategies))]
    public void Should_save_existing_hotel_data(HotelRepositoryStrategy strategy)
    {
        using var _ = strategy.Initialise();
        var sut = strategy.GetSubjectUnderTest();
        
        var hotelId = Guid.Parse("4204ace3-8497-4c4b-8a23-91d874781b36");
        var newHotel = new Hotel(hotelId, "Hotel Europe");
        sut.Save(newHotel);

        var changedHotel = new Hotel(hotelId, "Hilton");
        sut.Save(changedHotel);

        var retrievedHotel = sut.Get(hotelId);
        retrievedHotel.ShouldDeepEqual(changedHotel);
    }

    public static IEnumerable HotelRepositoryStrategies
    {
        get
        {
            yield return new TestCaseData(new SQLiteHotelRepositoryStrategy());
            yield return new TestCaseData(new FakeHotelRepositoryStrategy());
        }
    }
}

public interface HotelRepositoryStrategy : IDisposable {
    HotelRepository GetSubjectUnderTest();
    IDisposable Initialise();
}

public sealed class SQLiteHotelRepositoryStrategy : HotelRepositoryStrategy
{
    private readonly HotelBookingTestDatabase _hotelBookingDatabase;
    private readonly SQLiteHotelRepository _sqliteHotelRepository;
    
    public SQLiteHotelRepositoryStrategy()
    {
        var sqliteConnectionString = SQLiteTestConnectionUrl.Build();
        
        _hotelBookingDatabase = new HotelBookingTestDatabase(sqliteConnectionString);
        _sqliteHotelRepository = new SQLiteHotelRepository(sqliteConnectionString);
    }
    
    public HotelRepository GetSubjectUnderTest()
    {
        return _sqliteHotelRepository;
    }

    public IDisposable Initialise()
    {
        _hotelBookingDatabase.Initialise();
        return this;
    }

    public void Dispose()
    {
        _hotelBookingDatabase.Cleanup();
    }
}

public sealed class FakeHotelRepositoryStrategy : HotelRepositoryStrategy
{
    private readonly FakeHotelRepository _fakeHotelRepository;
    
    public FakeHotelRepositoryStrategy()
    {
        _fakeHotelRepository = new FakeHotelRepository();
    }
    
    public HotelRepository GetSubjectUnderTest()
    {
        return _fakeHotelRepository;
    }

    public IDisposable Initialise()
    {
        return this;
    }

    public void Dispose()
    {}
}