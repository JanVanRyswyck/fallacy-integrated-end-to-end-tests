package be.principal.it.ports;

import be.principal.it.domain.Hotel;

import java.util.UUID;

public interface HotelRepository {

    // Retrieves a hotel for a specified identifier.
    // When found, returns a `Hotel` instance; otherwise a null reference is returned.
    Hotel get(UUID id);

    // Saves the data of the specified `Hotel` instance.
    void save(Hotel hotel);
}
