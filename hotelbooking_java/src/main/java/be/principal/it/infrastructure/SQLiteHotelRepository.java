package be.principal.it.infrastructure;

import be.principal.it.domain.Hotel;
import be.principal.it.ports.HotelRepository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;

public class SQLiteHotelRepository implements HotelRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public SQLiteHotelRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Hotel get(UUID id) {
        var sql = "SELECT Id, Name FROM Hotel WHERE Id = :id";
        var parameters = Map.of("id", id);

        try {
            return jdbcTemplate.queryForObject(sql, parameters, SQLiteHotelRepository::mapHotelFromResultSet);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void save(Hotel hotel) {
        if(exists(hotel)) {
            update(hotel);
        } else {
            insert(hotel);
        }
    }

    private static Hotel mapHotelFromResultSet(ResultSet resultSet, int rowNumber) throws SQLException {

        return new Hotel(
            UUID.fromString(resultSet.getString("Id")),
            resultSet.getString("Name")
        );
    }

    private boolean exists(Hotel hotel) {

        var sql = "SELECT 1 FROM Hotel WHERE Id = :id";
        var parameters = Map.of("id", hotel.getId());

        try {
            var result = jdbcTemplate.queryForObject(sql, parameters, Integer.class);
            return result != null && result == 1;
        } catch (EmptyResultDataAccessException e) {
            return false;
        }
    }

    private void insert(Hotel hotel) {

        var sql = "INSERT INTO Hotel (Id, Name) VALUES (:id, :name)";

        var parameters = Map.of(
            "id", hotel.getId(),
            "name", hotel.getName());

        jdbcTemplate.update(sql, parameters);
    }

    private void update(Hotel hotel) {

        var sql = "UPDATE Hotel SET Name = :name WHERE Id = :id";

        var parameters = Map.of(
            "id", hotel.getId(),
            "name", hotel.getName());

        jdbcTemplate.update(sql, parameters);
    }
}
