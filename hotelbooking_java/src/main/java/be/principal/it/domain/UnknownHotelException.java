package be.principal.it.domain;

public class UnknownHotelException extends Exception {

    public UnknownHotelException(String message) {
        super(message);
    }
}
