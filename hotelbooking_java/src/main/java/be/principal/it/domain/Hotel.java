package be.principal.it.domain;

import java.util.UUID;

public class Hotel {

    private final UUID id;
    private String name;

    public Hotel(UUID id, String Name) {

        this.id = id;
        name = Name;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void changeName(String name) {
        this.name = name;
    }
}
