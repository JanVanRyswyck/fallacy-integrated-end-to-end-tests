package be.principal.it.domain;

import be.principal.it.ports.HotelRepository;

import java.util.UUID;

public class ChangeHotelNameHandler {

    private final HotelRepository hotelRepository;

    public ChangeHotelNameHandler(HotelRepository hotelRepository) {

        this.hotelRepository = hotelRepository;
    }

    public void changeHotelName(UUID hotelId, String newHotelName) throws UnknownHotelException {

        var hotel = hotelRepository.get(hotelId);
        if (hotel == null) {
            throw new UnknownHotelException("Unknown hotel.");
        }

        hotel.changeName(newHotelName);
        hotelRepository.save(hotel);
    }
}
