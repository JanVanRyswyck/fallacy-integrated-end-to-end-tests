package be.principal.it;

import be.principal.it.domain.Hotel;
import be.principal.it.ports.HotelRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FakeHotelRepository implements HotelRepository {

    private final Map<UUID, Hotel> hotels;

    public FakeHotelRepository() {
        hotels = new HashMap<>();
    }

    @Override
    public Hotel get(UUID id) {
        return hotels.get(id);
    }

    @Override
    public void save(Hotel hotel) {
        hotels.put(hotel.getId(), hotel);
    }
}
