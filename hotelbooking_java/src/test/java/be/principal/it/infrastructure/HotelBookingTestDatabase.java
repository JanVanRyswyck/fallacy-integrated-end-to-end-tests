package be.principal.it.infrastructure;

import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;

public class HotelBookingTestDatabase {

    private final JdbcTemplate jdbcTemplate;

    public HotelBookingTestDatabase(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void initialise() {
        var initialiseSchemaResource = new ClassPathResource("sql/InitialiseSchema.sql");
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator(initialiseSchemaResource);
        databasePopulator.execute(getDataSource());
    }

    public void cleanup() {
        var cleanupSchemaResource = new ClassPathResource("sql/CleanupSchema.sql");
        ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator(cleanupSchemaResource);
        databasePopulator.execute(getDataSource());
    }

    private DataSource getDataSource() {
        return jdbcTemplate.getDataSource();
    }
}
