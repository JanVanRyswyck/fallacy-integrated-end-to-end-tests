package be.principal.it.infrastructure.abstracttestcase;

import be.principal.it.infrastructure.SQLiteTestConnectionURL;
import be.principal.it.infrastructure.HotelBookingTestDatabase;
import be.principal.it.infrastructure.SQLiteHotelRepository;
import be.principal.it.ports.HotelRepository;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.sqlite.SQLiteDataSource;

public class SQLiteHotelRepositoryTests extends HotelRepositoryTests {

    private final HotelBookingTestDatabase hotelBookingDatabase;
    private final SQLiteHotelRepository sqliteHotelRepository;

    public SQLiteHotelRepositoryTests() {
        var sqliteDataSource = new SQLiteDataSource();
        sqliteDataSource.setUrl(SQLiteTestConnectionURL.build());

        var jdbcTemplate = new NamedParameterJdbcTemplate(sqliteDataSource);

        hotelBookingDatabase = new HotelBookingTestDatabase(jdbcTemplate.getJdbcTemplate());
        hotelBookingDatabase.initialise();

        sqliteHotelRepository = new SQLiteHotelRepository(jdbcTemplate);
    }

    @Override
    HotelRepository getSubjectUnderTest() {
        return sqliteHotelRepository;
    }

    @Override
    void cleanup() {
        hotelBookingDatabase.cleanup();
    }
}
