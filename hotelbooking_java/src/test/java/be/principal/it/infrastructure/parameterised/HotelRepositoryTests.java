package be.principal.it.infrastructure.parameterised;

import be.principal.it.FakeHotelRepository;
import be.principal.it.infrastructure.SQLiteTestConnectionURL;
import be.principal.it.infrastructure.HotelBookingTestDatabase;
import be.principal.it.infrastructure.SQLiteHotelRepository;
import be.principal.it.domain.Hotel;
import be.principal.it.ports.HotelRepository;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.sqlite.SQLiteDataSource;

import java.util.UUID;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class HotelRepositoryTests {

    @ParameterizedTest
    @ArgumentsSource(HotelRepositoryStrategyProvider.class)
    public void Should_return_hotel_data_for_identifier(HotelRepositoryStrategy strategy) {

        var SUT = strategy.getSubjectUnderTest();

        var hotelId = UUID.fromString("f24d22fc-25b6-47d6-ad52-07f624b462eb");
        var expectedHotel = new Hotel(hotelId, "Hotel Europe");
        SUT.save(expectedHotel);

        var retrievedHotel = SUT.get(hotelId);
        assertThat(retrievedHotel).usingRecursiveComparison().isEqualTo(expectedHotel);
    }

    @ParameterizedTest
    @ArgumentsSource(HotelRepositoryStrategyProvider.class)
    public void Should_return_nothing_for_a_non_existing_hotel(HotelRepositoryStrategy strategy) {

        var SUT = strategy.getSubjectUnderTest();

        var unknownHotelId = UUID.fromString("9529f607-b1d2-4597-9b7e-26e4ba29d196");
        var retrievedHotel = SUT.get(unknownHotelId);
        assertThat(retrievedHotel).isNull();
    }

    @ParameterizedTest
    @ArgumentsSource(HotelRepositoryStrategyProvider.class)
    public void Should_save_new_hotel_data(HotelRepositoryStrategy strategy) {

        var SUT = strategy.getSubjectUnderTest();

        var hotelId = UUID.fromString("c11cf51a-42fa-4480-add5-a7d0809a8973");
        var newHotel = new Hotel(hotelId, "Hotel Europe");
        SUT.save(newHotel);

        var retrievedHotel = SUT.get(hotelId);
        assertThat(retrievedHotel).usingRecursiveComparison().isEqualTo(newHotel);
    }

    @ParameterizedTest
    @ArgumentsSource(HotelRepositoryStrategyProvider.class)
    public void Should_save_existing_hotel_data(HotelRepositoryStrategy strategy) {

        var SUT = strategy.getSubjectUnderTest();

        var hotelId = UUID.fromString("4204ace3-8497-4c4b-8a23-91d874781b36");
        var newHotel = new Hotel(hotelId, "Hotel Europe");
        SUT.save(newHotel);

        var changedHotel = new Hotel(hotelId, "Hilton");
        SUT.save(changedHotel);

        var retrievedHotel = SUT.get(hotelId);
        assertThat(retrievedHotel).usingRecursiveComparison().isEqualTo(changedHotel);
    }

    static class HotelRepositoryStrategyProvider implements ArgumentsProvider {

        @Override
        public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
            return Stream.of(
                Arguments.of(new SQLiteHotelRepositoryStrategy()),
                Arguments.of(new FakeHotelRepositoryStrategy())
            );
        }
    }

    interface HotelRepositoryStrategy extends AutoCloseable {
        HotelRepository getSubjectUnderTest();
    }

    static class SQLiteHotelRepositoryStrategy implements HotelRepositoryStrategy {

        private final HotelBookingTestDatabase hotelBookingDatabase;
        private final SQLiteHotelRepository sqliteHotelRepository;

        public SQLiteHotelRepositoryStrategy() {

            var sqliteDataSource = new SQLiteDataSource();
            sqliteDataSource.setUrl(SQLiteTestConnectionURL.build());

            var jdbcTemplate = new NamedParameterJdbcTemplate(sqliteDataSource);

            hotelBookingDatabase = new HotelBookingTestDatabase(jdbcTemplate.getJdbcTemplate());
            hotelBookingDatabase.initialise();

            sqliteHotelRepository = new SQLiteHotelRepository(jdbcTemplate);
        }

        @Override
        public HotelRepository getSubjectUnderTest() {
            return sqliteHotelRepository;
        }

        @Override
        public void close() {
            hotelBookingDatabase.cleanup();
        }
    }

    static class FakeHotelRepositoryStrategy implements HotelRepositoryStrategy {

        private final FakeHotelRepository fakeHotelRepository;

        public FakeHotelRepositoryStrategy() {
            fakeHotelRepository = new FakeHotelRepository();
        }

        @Override
        public HotelRepository getSubjectUnderTest() {
            return fakeHotelRepository;
        }

        @Override
        public void close() {
        }
    }
}
