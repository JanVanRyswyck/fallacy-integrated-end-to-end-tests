package be.principal.it.infrastructure.abstracttestcase;

import be.principal.it.domain.Hotel;
import be.principal.it.ports.HotelRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

abstract class HotelRepositoryTests {

    private HotelRepository SUT;

    @BeforeEach
    public void setUp() {
        SUT = getSubjectUnderTest();
    }

    @AfterEach
    public void tearDown() {
        cleanup();
    }

    @Test
    public void Should_return_hotel_data_for_identifier() {

        var hotelId = UUID.fromString("f24d22fc-25b6-47d6-ad52-07f624b462eb");
        var expectedHotel = new Hotel(hotelId, "Hotel Europe");
        SUT.save(expectedHotel);

        var retrievedHotel = SUT.get(hotelId);
        assertThat(retrievedHotel).usingRecursiveComparison().isEqualTo(expectedHotel);
    }

    @Test
    public void Should_return_nothing_for_a_non_existing_hotel() {

        var unknownHotelId = UUID.fromString("9529f607-b1d2-4597-9b7e-26e4ba29d196");
        var retrievedHotel = SUT.get(unknownHotelId);
        assertThat(retrievedHotel).isNull();
    }

    @Test
    public void Should_save_new_hotel_data() {

        var hotelId = UUID.fromString("c11cf51a-42fa-4480-add5-a7d0809a8973");
        var newHotel = new Hotel(hotelId, "Hotel Europe");
        SUT.save(newHotel);

        var retrievedHotel = SUT.get(hotelId);
        assertThat(retrievedHotel).usingRecursiveComparison().isEqualTo(newHotel);
    }

    @Test
    public void Should_save_existing_hotel_data() {

        var hotelId = UUID.fromString("4204ace3-8497-4c4b-8a23-91d874781b36");
        var newHotel = new Hotel(hotelId, "Hotel Europe");
        SUT.save(newHotel);

        var changedHotel = new Hotel(hotelId, "Hilton");
        SUT.save(changedHotel);

        var retrievedHotel = SUT.get(hotelId);
        assertThat(retrievedHotel).usingRecursiveComparison().isEqualTo(changedHotel);
    }

    abstract HotelRepository getSubjectUnderTest();
    abstract void cleanup();
}
