package be.principal.it.infrastructure.abstracttestcase;

import be.principal.it.FakeHotelRepository;
import be.principal.it.ports.HotelRepository;

public class FakeHotelRepositoryTests extends HotelRepositoryTests {

    private final FakeHotelRepository fakeHotelRepository;

    public FakeHotelRepositoryTests() {
        fakeHotelRepository = new FakeHotelRepository();
    }

    @Override
    HotelRepository getSubjectUnderTest() {
        return fakeHotelRepository;
    }

    @Override
    void cleanup() {
    }
}
