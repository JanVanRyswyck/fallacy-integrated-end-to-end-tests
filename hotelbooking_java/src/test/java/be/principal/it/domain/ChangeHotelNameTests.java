package be.principal.it.domain;

import be.principal.it.FakeHotelRepository;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class ChangeHotelNameTests {

    @Test
    public void Should_change_the_hotel_name() throws Exception {

        var hotelId = UUID.fromString("33701626-ad1c-4a30-9fd0-4cb891ac442c");
        var hotel = new Hotel(hotelId, "The Sphere");

        var hotelRepository = new FakeHotelRepository();
        hotelRepository.save(hotel);

        var handler = new ChangeHotelNameHandler(hotelRepository);
        handler.changeHotelName(hotelId, "The New Sphere");

        var storedHotel = hotelRepository.get(hotelId);
        assertThat(storedHotel.getName()).isEqualTo("The New Sphere");
    }

    @Test
    public void Should_throw_exception_for_an_unknown_hotel() {

        var hotelId = UUID.fromString("f688a902-c2f7-48c3-9cf2-29b4b190e21f");
        var handler = new ChangeHotelNameHandler(new FakeHotelRepository());

        assertThatExceptionOfType(UnknownHotelException.class)
            .isThrownBy(() -> handler.changeHotelName(hotelId, "The New Sphere"));
    }
}